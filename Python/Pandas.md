# Pandas

[TOC]

## 安装

- anaconda 安装:
  - conda list pandas 查看是否已经安装
  - conda install pandas
  - conda update pandas
- pip 安装
  - pip install pandas
- apt 安装
  - sudo apt-get install python-pandas
## 测试是否安装成功
> nosetests pandases

不成功会进行提醒，可以重新安装或者更新

## pandas数据结构

- Series
- DataFrame


## Series

### 1 声明series 对象

```
s = pd.Series([12,3,9,1])
s
```

```
s = pd.Series([12,52,4,2],index=['a','b','c','d'])
s.values
s.index
```

### 2 用numpy数组或其他对象定义

```
arr=np.array([1,2,3])
s=pd.Series(arr)
# 注意这里的s对象中的元素是对arr的引用，如果改变arr的值，s的值也会改变

# 用作字典
dic={'red':123,'blue',122,'green':129}
s.Series(dic)
```

### 3 筛选元素

```
s[s>8] # 对values的比较
s/2 
np.log(s)
```

### 4 其他函数

```
s.unique() # 只出现一次
s.value_counts() # 统计数量
# 做筛选
s.isin([0,3])
s[s.isin([0,3])]
# 空和不空
s.isnull()
s.notnull()
```

## DataFrame

> 和excel类似
### 创建Frame

```
data={'title1':[1,2,3],'title2':[5,4,3]}
frame=pd.DataFrame(data)
frame=pd.DataFrame(data,columns=['title1'])

# index 会自动生成索引也可以通过index属性进行更改
pd.DataFrame(data,index=['one','two','three']) # 个数需要匹配
```

### 选取元素

```
frame.columns
frame.index
frame.values

frame['title1'] or frame.title1
# 行
frame.ix[2]
frame.ix[[0,1]]
frame[0:2]
frame['title1'][2]
```

### 赋值

```
frame.index.name='id'
frame.columns.name='cols'
frame['title1'][3]= 3
ser=pd.Series(np.arange(5))
frame['new']=ser
```

### 其他

```
# 所属关系
frame.isin([1.0,'a'])
# 删除一列
del frame['new']
# 筛选
frame[frame <= 2]
# 转置
frame.T
```

## Index 对象

```
ser.idxmin()
ser.idxmax()
ser.index.is_unique()

# 执行更换索引
ser.reindex(['a','b','c'])

# 删除
ser.drop('0')
```

## 算数运算方法

```
frame1.add(frame2)
sub(), div(), mul()
```

### DataFrame 与 Series的运算

```
frame=pd.DataFrame(np.arange(16).reshape(4,4),index=['red','blue','gren','yellow'],columns=['ball','pen','paper','socket'])

ser=pd.Series(np.arange(4),index=['ball','pen','paper','socket'])

frame - ser
```



## Pandas 库函数

###　按行或列执行

>  f=lambda x:x.max() - x.min()
>
>  def f(x):
>
>  ​	return x.max()-x.min()
>
>  frame.apply(f,axis=1) # 处理行

### 函数

```
sum()
mean()
describe() # 计算多个统计量
# 排序函数
sort_index([ascending=False][axis=1])
```

#### 对数据结构中的元素排序

```
# series
ser.order()
# DataFrame
frame.sort_index(by='pen')
frame.sort_index(by=['pen','pencil'])
```

##### 位次排序

- ser.rank()
- ser.rank(method='first')
- ser.rank(ascending=False)

##### 相关性和协方差

correlation and covariance 计算通常涉及到两个Series 对象

```
>>> seq2=pd.Series([3,4,3,4,5,4,3,2],['06','07','08','09','10','11','12','13'])
>>> seq1=pd.Series([1,2,3,4,4,3,2,1],['06','07','08','09','10','11','12','13'])
>>> seq1.corr(seq1)
1.0
>>> seq1.corr(seq2)
0.7745966692414835
>>> seq1.cov(seq2)
0.8571428571428571
```

DataFrame 可以实现其对象的相关性和协方差,返回DataFrame矩阵

```
>>> frame2=pd.DataFrame([[1,4,3,6],[4,5,6,1],[3,3,1,5],[4,1,6,4]],index=['r','g','b','w'],columns=['ball','pen','pencil','paper'])
>>> frame2.corr()
            ball       pen    pencil     paper
ball    1.000000 -0.276026  0.577350 -0.763763
pen    -0.276026  1.000000 -0.079682 -0.361403
pencil  0.577350 -0.079682  1.000000 -0.692935
paper  -0.763763 -0.361403 -0.692935  1.000000
>>> frame2.cov()
            ball       pen    pencil     paper
ball    2.000000 -0.666667  2.000000 -2.333333
pen    -0.666667  2.916667 -0.333333 -1.333333
pencil  2.000000 -0.333333  6.000000 -3.666667
paper  -2.333333 -1.333333 -3.666667  4.666667
```

除此之外还可以计算DataFrame对象与Series或者DataFrame对象之间的关系

> frame.corrwith(ser)
>
> frame.corrwith(frame2)

## NaN

### 过滤NaN

```
# series obj
ser.dropna()
or
ser[ser.notnull()]

# DataFrame obj
frame.dropna() # 整行都被删除
frame.dropna(how='all') # 删除全部Nan的列
```

### 为NaN填充

```
frame.fillna(0)
frame.filena('ball':1,'mug':2)
```

## 等级索引&分级

```
>>> mser=pd.Series(np.random.rand(6),[['a','a','b','b','c','c'],[2,3,6,4,5,8]])
>>> mser
a  2    0.936455
   3    0.431867
b  6    0.996895
   4    0.584551
c  5    0.883911
   8    0.049483
dtype: float64
# 选取
mser['a'][2]=0.936455
```

### DataFrame 与　Series转化

DataFrame -> Series : stack()

```
frame.stack()
```

Series -> DataFrame : unstack()

```
mser.unstack()
```

交换层级:

```
frame.colums.names=['a','b']
frame.index.name=['c','d']
frame.swaplevel('c','d')
frame.sortlevel('c')# 只按照一个层级进行排序

# 按层级统计数据
frame.sum(level='c')
frame.sum(level='c',axis=1)
```

## Pandas 数据读写

### 读

---

- read_csv()
- read_excel()
- read_sql()
- read_json()
- read_html()
- read_clipboard()

----

### 写

---

- to_csv()
- to_excel()
- to_sql()
- to_json()
- to_html()
- to_clipboard()

---



### CSV 文件

>1. read_csv()
>2. read_table()
>3. to_csv()



```
pd.csvframe=read_csv('data.csv')
pd.read_table('data.csv',sep=',')
# names 指定表头
read_csv('data.csv',names=['id'])
# header 添加表头
```

### TxT RegExp 解析

> 在read_table中使用sep中的正则表达式

| .    | 单个字符  |
| ---- | ----- |
| \d   | 数字    |
| \D   | 非数字   |
| \s   | 空白字符  |
| \S   | 非空白字符 |
| \n   | 换行符   |
| \t   | 制表符   |

```
>>> pd.read_table('tt.txt',sep='\s*')
   white  red  blue  green
0      1    5     2      3
1      2    7     8      5
2      3    3     6      7
>>> pd.read_table('tt.txt',sep='\D*',header=None)
 # skiprows=[1,3,6]排除1,3,6 行
 # skiprows=5 排除前五行
```

### 部分数据读取(常用用法)

从 skiprows 开始读，读多少 nrows行

> read_csv('data.csv',skiprows=[2],nrows=3,header=None)

解析文本：

```
out=pd.Series()
i=0
# chunksize 一次处理数据行数
pieces = pd.read_csv('tips.csv',chunksize=3)
for piece in pieces:
	out.set_value(i,piece['size'].sum())
	i=i+1
```

### 往CSV中写数据

```
frame.to_csv('tt.csv',index=False,header=False)
```

替换，，中内容

>frame.to_csv('new.csv',na_rep='NaN')

## HTML

### 读取HTML

> read_html()
>
> to_html()

安装html5lib库

```
conda install html5lib
```

>tf=pd.DataFrame(np.arange(4).reshape(2,2))
>
>print(tf.to_html()) # 自动转化html

更加复杂的例子：(生成)

```python
frame=pd.DataFrame(np.random.random((4,4)),index=['white','black','red','blue'],columns=['up','down','right','left'])

s=['<html>']
s.append('<head><title>DataFrame</title></head>')
s.append('<body>')
s.append(frame.to_html())
s.append('</body></html>')
html=''.join(s)

html_file=open('myFrame.html','w')
html_file.write(html)
html_file.close()
```

打开HTML文件：

```
>>> web_frame=pd.read_html('myFrame.html')
>>> web_frame
[  Unnamed: 0        up      down     right      left
0      white  0.654247  0.917280  0.599515  0.401334
1      black  0.647599  0.555069  0.612985  0.279210
2        red  0.204044  0.719828  0.950169  0.749252
3       blue  0.089194  0.498193  0.568361  0.427534]
```

局限于读表格

## 从XML读取数据

> lxml 库文件

```python
from lxml import objectify
xml=objectify.parse('test.xml')
root=xml.getroot()
```

### 读写excel文件

```
pd.read_excel('data.xlsx')
pd.read_excel('data.xlsx','sheet2')
```



# 深入pandas 数据处理

### 三个阶段

- 数据准备
- 数据转化
- 数据聚合

## 数据准备

- 加载
- 组装
  - 合并 - pandas.merge()
  - 拼接 - pandas.concat()
  - 组合 - pandas.DataFrame.combine_first()
- 变形
- 删除

### 合并

example1:

```
import numpy as np
import pandas as pd
frame1 = pd.DataFrame({'id':['ball','pencil','pen','mug','ashtray'],'price':[12.33,11.44,33.21,13.23,33.62]})
frame2 = pd.DataFrame({'id':['pencil','ball','pencil','pen'],'color':['white','red','red','black']})
pd.merge(frame1,frame2)
```

有必要定义合并操作的标准 用 on 来指定

example2: 

> frame2.columns=['brand2','id2']
>
> pd.merge(frame1,frame2,on='brand') # 需要重新明明
>
> pd.merge(frame1,frame2,right_on='brand', left_on='sid')

### 拼接

concatenation

numpy 的 concatenate()函数就是做这种拼接操作

```python
array1=np.arange(9).reshape((3,3))
array2=np.arange(9).reshape((3,3))+6
np=concatenate([array1,array2],axis=1)# axis=1 从行拼接 axis=0 从列拼接
```

pandas的concat()函数可以做拼接操作

```python
ser1=pd.concat([ser1,ser2])
# axis=1 从行拼接 axis=0 从列拼接
# join='inner' or 'outer'
```

### 组合

 Series对象： combine_first()

组合的同时还可以对齐数据

```
ser1=pd.Series(np.random.rand(5),index=[1,2,3,4,5])
ser2=pd.Series(np.random.rand(4),index=[2,4,5,6])
ser1.combine_first(ser2)
```

### 轴向旋转

意思是 需要按照行重新调整列或者反过来

两个操作：

- stacking 入栈， 把列转化为行
- unstacking 出站， 把行转化为列

```python
frame1=pd.DataFrame(np.arange(9).reshape(3,3),index=['w','b','r'], columns=['ball','pen','pencil'])
frame1.stack() # 得到一个Series对象
ser.unstack() # 得到一个DataFrame对象

# 长格式向宽格式转化： DateFrame.pivot
wideframe=longframe.pivot('color','item')
```

### 删除

- 删除一列

>  del frame['ball']

- 删除多余的行

> frame.drop('white')

 ## 数据转化

### 删除重复数据

DataFrame 中duplicated()函数可以用来检测重复的行，返回bool型Series对象

```
dframe.duplicated()
# 得到过滤结果
dframe[dframe.duplicated()]
# 讲重复的行删除
dframe.drop_duplicates<>
```

### 映射

dict 映射关系比较好

```python
replace() 替换元素
map() 新建一列
rename() 替换索引

### 替换
newcolor={'rosso':'red','verde':'green'}
frame.replace(newcolors)

ser.replace(np.nan, 0)

### 添加元素
price={'ball':5.56,'mug':4.3}
frame['price']=frame['item'].map(price)

### 重命名轴索引
reindex={o:'first',2:'second'}
frame.replace(reindex)
frame.replace(index={1:'first'}, columns={'item':'object'})
# inplace 参数： 是否改变调用函数对象本身
```

### 离散化

```
result=[12,34,67,55,28,90.99,12,3,56,74,44,87,23,49,89,87]
bins=[0,25,50,75,100]
# 对result用cut函数
cat=pd.cut(result,bins)
cat >>> type(cat)
<class 'pandas.core.categorical.Categorical'>
# 返回的是类别对象
cat.levels
cat.labels
# 类别中计数
pd.value_counts(cat)
# cut 函数中的labels标签 labels=['a','b','c']
```

### 异常值的检测和过滤

```
randframe=pd.DataFrame(np.random.randn(1000,3))
```

descibe()函数查看每一列的描述性统计量

假设讲比标准差大三倍的元素是为异常值，用std()函数可以求出每一列的标准差

```
randframe.std()
```

对DataFrame对象进行过滤

```
randframe[(np.abs(randframe)>(3*randframe.std())).any(1)]
```

### 排序

```python
nframe=pd.DataFrame(np.arange(25).reshape(5,5))
# permutation(5)创建一个随机顺序整数
new_order=np.random.permutation(5) # 0-4
nframe.take(new_order)
```

随机取样

```
np.random.randint()函数
sample=np.random.randint(0,len(nframe),size=3)
```



## 字符串处理

### 内置字符串处理方法

> split() 函数 切割

```python
test='12312,bob'
test.split(',')
# ['12312', 'bob']
```

> strip()函数 去空白

```
tokens=[s.strip() for s in test.split(',')]
```

> join() 拼接

```
>>> strings=['1','2','3','45','5']
','.join(strings)
```

> in index() find() 查找操作

```
test.index('bottom')
test.find('bottom')
'bottom' in test
```

> count() 出现次数

```
test.count('bottom')
```

> replace()

```
test.replace('A','a')
```

### 正则表达式

```
import re
```

几个类别:

- 模式匹配
- 替换
- 切分

> re.split()

```
text="This is        an \t odd \n text!"
re.split('\s+',text)

# 内部过程
regex=re.compile('\s+')
regex.split(text)
```

> re.findall()

```
# 以A开头不区分大小写
text='A! This is my address: 16 Boltom Avenue, Boston'
re.findall('[A,a]\w+',text)
```

## 数据聚合

### GroupBy

SPLIT-APPLY-COMBINE 三个阶段

- 分组
- 用函数处理
- 合并

```python
# 实际上只使用了GroupBy函数
 frame=pd.DataFrame({'color':['white','red','green','red','green'],'obj':['pen','pencil','pencil','ashtray','pen'],'price1':[5.56,4.20,1.3,0.56,2.75],'price2':[4.75,4.12,1.60,0.75,3.15]})

  >>> frame
   color      obj  price1  price2
0  white      pen    5.56    4.75
1    red   pencil    4.20    4.12
2  green   pencil    1.30    1.60
3    red  ashtray    0.56    0.75
4  green      pen    2.75    3.15


# 想要根据color组，计算price1的均值
group=frame['price1'].groupby(frame['color'])
# 得到一个group对象
group.groups # 查看分组情况
group.mean() # 查看均值
group.sum() # 查看分组总和
```

### 等级分组

> ggroup=frame['price1'].groupby([frame['color'],frame['obj']])
>
> frame[['price1','price2']].groupby(frame['color']).mean()

### 组迭代

```
for name, group in frame.groupby('color'):
	print(name)
	print(group)
```

### 分组函数

```
group=frame.groupby('color')
group['price1'].quantile(0.6) # 直接计算分位数

# 自定义聚合函数
def range(series):
	return series.max()-series.min()
group['price1'].agg(range)

group.agg(range)
```

