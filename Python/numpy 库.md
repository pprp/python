# numpy 库

[TOC]

## ndarray : numpy 的关键

    a = np.array([1,2,3])
    # 轴
    a.ndim
    # 数组长度
    a.size
    # 数组的型
    a.shape
    # 类型
    a.dtype

### 创建数组
    a = np.array([1,2,3],[2,3,4])
    b = np.array((1,2,3),(4,5,6))
    c = np.array([1,2,3],(4,5,6))
    
    d = np.zeros((3,3))
    e = np.ones((3,3))
    f = np.arange(3,14)
    g = np.arange(0,12,3)
    h = np.arange(0,12).reshape(3,4)
    i = np.linspace(0,10,5)
    j = np.random.random((3,5))

### 基本操作

#### 算数运算符

    # 对每个元素操作
    a=np.array(4)
    a+4
    a*2
    
    b=np.array(4,8)
    a+b
    a-b
    a*np.sin(b)
    a*np.sqrt(b)
    
    # ++ --
    自增自减
    元素级（对每个元素起作用）


### 矩阵积

    # 不是元素级别的
    np.dot(A,B) != np.dot(B,A)

### 通用函数与聚合函数

    # 通用函数
    a=np.array(3,4)
    np.sqrt(a)
    np.log(a)
    np.sin(a)
    
    # 聚合函数
    a.sum()
    a.min()
    a.max()
    a.mean()
    a.std()

### 切片操作

    a=np.arange(3,4)
    # 从第二个到第六个元素（=1 && < 5）
    a[1:5]
    # 每两个抽取一个
    a[1:5:2]
    # 起始位置 ： 结束位置 ： 切片间隔
    a[::2]
    a[:5:2]
    a[:5:]
    # 取某一行
    a[0,:]
    # 取某一列
    a[:,0]

### 遍历

    for i in a:
        print(i)
    
    for i in a.flat:
        print(i)
        
    np.apply_along_axis(func,axis=0,arr=A)

### 形状改变

    # 改为2*2
    a=arange(0,4).reshape(2,2)
    
    # 改为一维数组
    a.ravel()
    
    # 转置
    a.transpose()

### 数组的链接

    #垂直入栈
    np.vstack((A,B))
    #水平入栈
    np.hstack((A,B))
    
    #多个数组之间的站操作
    np.column_stack((a,b,c))
    np.row_stack((a,b,c))

### 数组切分

    A=np.arange(16).reshape((4,4))
    # 按照列进行平分
    [B,C]=np.hsplit(A,2)
    # 按照行进行平分
    [B,C=np.vsplit(A,2)
    
    # split 
    传入数组作为参数：指定被切分部分索引，axis=1 列索引，axis=0 行索引
## 读取CSV

```
data = genfromtxt('data.csv',delimeter=',',names=True)
```

