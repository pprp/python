# Caffee 安装过程

[TOC]



1.安装依赖包

```
$ sudo apt-get install libprotobuf-dev libleveldb-dev libsnappy-dev libopencv-dev libhdf5-serial-dev protobuf-compiler
$ sudo apt-get install --no-install-recommends libboost-all-dev
$ sudo apt-get install libopenblas-dev liblapack-dev libatlas-base-dev
$ sudo apt-get install libgflags-dev libgoogle-glog-dev liblmdb-dev
```

2.安装caffe

> ```
> $ git clone git://github.com/BVLC/caffe.git
> ```

3.编译caffe

```
$ cd caffe
$ cp Makefile.config.example Makefile.config (复制一份Makefile.config文件)
```

在Makefile.config文件中把CPU_ONLY := 1的注释给去掉，就是去掉前面的#号

接下来在caffe根目录下执行下面命令

> ```
> $ make all
> ```

在这个命令后，我遇到了一个报错信息，

```
./include/caffe/util/hdf5.hpp:6:18: fatal error: hdf5.h: No such file or directory
```

这是hdf5路径问题造成的，可以通过下面命令来获得hdf5的路径，

```
$ sudo find / -name hdf5.h 
```

我找到的hdf5.h的路径为：/usr/include/hdf5/serial/hdf5.h，于是在makefile.config文件中，把文件改成下面所示：

再执行一遍上述命令，继续报错，这次是/usr/bin/ld: cannot find -lhdf5 
于是同上面一个处理 去找libhdf5.so 
配置文件改为：

> LIBRARY_DIRS := $(PYTHON_LIB) /usr/lib/x86_64-linux-gnu/hdf5/serial \
> ​            /usr/local/lib /usr/lib

> INCLUDE_DIRS := $(PYTHON_INCLUDE) /usr/include/hdf5/serial \
> ​            /usr/local/include

> /usr/bin/ld: cannot find -lcblas
> /usr/bin/ld: cannot find -latlas

解决

```
sudo apt-get install libatlas-base-dev
```

接着执行下面命令：

```
make test
```

最后，

```
$ make runtest
```

4.编译python接口

安装pip

```
$ sudo apt-get install python-pip
```

执行安装依赖 
根据caffe/python目录下的requirements.txt中列出的清单安装即可。 
fortran编译器（gfortran)使为了安装scipy，否则会报错。

```
cd ~/caffe
sudo apt-get install gfortran
cd ./python
for req in $(cat requirements.txt); do pip install $req; done
```

回到caffe根目录

```
sudo pip install -r python/requirements.txt
```

编译pycaffe接口

```
make pycaffe -j81
```

此时报错

```
python/caffe/_caffe.cpp:10:31: fatal error: numpy/arrayobject.h: 没有那个文件或目录
12
```

于是，输入如下命令即可

```
sudo apt-get install python-numpy
```
```
import sys
sys.path.append("/（你的caffe路径）/caffe/python")
sys.path.append("/（你的caffe路径）/caffe/python/caffe")
```



PS:永久性添加python路径：

```
cd /usr/lib/python2.7/dist-packages 
sudo echo mycaffe.pth    #建立一个mycaffe.pth文件
sudo gedit mycaffe.pth   #编辑文件
```

开始python2 import caffe

报错：ImportError: No module named skimage.io

解决：

```
sudo apt-get install python-skimage
```

报错：ImportError: No module named google.protobuf.internal

```
conda install protobuf
```

还是没有解决：

接着找：

```
sudo apt-get install python-protobuf
```

成功了！

接下来开始测试：

Mnist 数据集获取：

```
sudo sh ./data/mnist/get_mnist.sh
sudo sh ./examples/mnist/create_mnist.sh
```

2、修改配置

修改该目录下的prototxt扩展名配置文件 
修改./examples/mnist/lenet_solver.prototxt 
定位到最后一行：solver_mode: GPU，将GPU改为CPU。 使用CPU进行测试

3、运行 
执行文件命令：

```
sudo sh ./examples/mnist/train_lenet.sh
```

报错：./train_lenet.sh: 4: ./train_lenet.sh: ./build/tools/caffe: not found

重新编译

```
sudo make all
```

显示： nothing to be done for all

最终训练完的模型存储为一个二进制的protobuf文件： 
　　　　./examples/mnist/lenet_iter_10000.caffemodel

这个模型就可以用来直接使用了

突然就可以使用了。。。

重新执行 

```
sudo sh ./examples/mnist/train_lenet.sh
```

最好加time 看看所用时间

```
time sudo sh ./examples/mnist/train_lenet.sh
```



训练花了大概15分钟，挺快的，然后用训练好的模型对数据进行预测

```
./build/examples/cpp_classification/classification.bin examples/mnist/deploy.prototxt examples/mnist/lenet_iter_10000.caffemodel examples/mnist/mean.binaryproto examples/mnist/synset_words.txt examples/images/3.jpeg
```

图片如下：

![image](https://img-blog.csdn.net/20170612172619512?watermark/2/text/aHR0cDovL2Jsb2cuY3Nkbi5uZXQveHVuYW4wMDM=/font/5a6L5L2T/fontsize/400/fill/I0JBQkFCMA==/dissolve/70/gravity/Center)

> 我们需要做的操作是将图片放到文件夹examples/images/3.jpeg

- 其实还缺少deploy.prototxt 文件，协议文件

在examples/mnist目录下复制一份lenet_train_test.prototxt修改并保存后得到deploy.prototxt

```
cp examples/mnist/lenet_train_test.prototxt ./examples/mnist/deploy.prototxt
```

- 生成labels.txt标签文件

​     在当前目录下新建一个txt文件，命名为synset_words.txt，里面内容为我们训练mnist的图片内容，共有0~9十个数

```
synset_words.txt
0
1
2
3
4
5
6
7
8
9
```

- 步骤4:生成mean.binaryproto二进制均值文件

​     均值文件分为二进制均值文件和python类均值文件。因为博主还未配置python接口，故未采用python方法，这也是之前一直出错的原因。

​     生成均值文件的两种方法请参看博文：[点击博文链接](http://blog.csdn.net/qq_26898461/article/details/50469789)

​     caffe作者为我们提供了一个计算均值的文件compute_image_mean.cpp，放在caffe根目录下的tools文件夹里面，运行下面命令生成mean.binaryproto二进制均值文件。

```
sudo build/tools/compute_image_mean examples/mnist/mnist_train_lmdb examples/mnist/mean.binaryproto  
```

生成的mean.binaryproto均值文件保存在了examples/mnist目录下。

- 步骤5：分类器classification.bin（windows下caffe为classification.exe）

  ​       在example文件夹中有一个cpp_classification的文件夹，打开它，有一个名为classification的cpp文件，这就是caffe提供给我们的调用分类网络进行前向计算，得到分类结果的接口。就是这个文件在命令中会得到classification.bin，具体我们可以不用管它，在其他caffemodel下不用修改也可以用，不像均值文件，不同的模型需要不同的均值文件。









cpu版本的比较简单，一点一点问题查阅博客是可以解决的

主要参考博客：

[博客1](https://blog.csdn.net/lanseyuansu/article/details/70173606)

[博客2](https://blog.csdn.net/zt_1995/article/details/56283249)

[博客3](https://blog.csdn.net/xunan003/article/details/73126425)